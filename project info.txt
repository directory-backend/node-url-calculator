Presentation link: https://www.youtube.com/watch?v=tzSKZ7GxbT8 

Implement the following JS scripts - one file should contain the implementation of one task.
Solutions in pure Node, without express
A simple HTTP server accepting requests to perform various mathematical operations (add, sub, mul, div) under different url.
Extracting parameters from the request parameters (can be in the path, for example) 
and returning the result in the form of an html response containing the request parameters and the result.
Formatting is up to you. Remember about handling wrong / missing parameters.