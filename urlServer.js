const http = require("http");
const url = require("url");

// url format
// add-> http://localhost:9000/add?v1=10&v2=20
// mul->http://localhost:9000/mul?v1=10&v2=20
// div->http://localhost:9000/div?v1=10&v2=20
// sub->http://localhost:9000/sub?v1=10&v2=20

const server = http.createServer(function(req, res) {
  const urlObject = url.parse(req.url, true);
  const queryData = urlObject.query; // {v1=10 v2=20}

  const v1 = parseFloat(queryData.v1);
  const v2 = parseFloat(queryData.v2);
  let operator = "";
  let score = "";

  switch (urlObject.pathname) {
    case "/add":
      daneOK = true;
      operator = new String("+");
      score = new String(v1 + v2);
      daneOK = true;
      break;

    case "/mul":
      daneOK = true;
      operator = "*";
      score = new String(v1 * v2);
      break;
    case "/div":
      daneOK = true;
      operator = "&#247;";
      score = new String(v1 / v2);
      break;
    case "/sub":
      daneOK = true;
      operator = "-";
      score = new String(v1 - v2);
      break;
    default:
      daneOK = false;

      console.log("Err: wrong data");
      break;
  }
  if (daneOK === true) {
    res.writeHead(200, { "Content-type": "text/html" });

    res.write(
      "<div align='center' style='font-size:10vw;background-color:lightblue'>" +
        "<h1>" +
        v1 +
        operator +
        v2 +
        "=" +
        score +
        "</h1>" +
        "</div>"
    );

    res.end();
  } else {
    res.writeHead(200, { "Content-type": "text/html" });

    res.write(
      "<div align='center' style='font-size:10vw;background-color:lightblue'>" +
        "<h1>" +
        "Err: wrong data" +
        "</h1>" +
        "</div>"
    );

    res.end();
  }
});

server.listen(9000, () => {
  console.log("Sever running on port 9000...");
});
